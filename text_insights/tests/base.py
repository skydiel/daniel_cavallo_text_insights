import os
from paste.deploy.loadwsgi import appconfig
from pyramid import testing


class BaseTest:
    @classmethod
    def setup_class(cls):
        cls.settings = appconfig('config:' + os.path.join(os.path.dirname(__file__), '../../test.ini'))
        cls.request = testing.DummyRequest()
        cls.config = testing.setUp(settings=cls.settings, request=cls.request)
