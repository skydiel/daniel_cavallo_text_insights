import pytest
from marshmallow import ValidationError

from text_insights.api import schemas
from text_insights.tests.base import BaseTest


class TestSearchApiEndpointSchema(BaseTest):

    def test_empty_dict_error(self):

        with pytest.raises(ValidationError):
            schemas.SearchEndpointSchema().load({})

    def test_no_required_key_error(self):

        params = {
            'search': 'some text',
            'engine': 'google'
        }

        with pytest.raises(ValidationError):
            schemas.SearchEndpointSchema().load(params)

    def test_invalid_key_error(self):

        params = {
            'q': 'some text',
            'provider': 'google',
            'hits': 1,
            'alien key': 'alien value'
        }

        with pytest.raises(ValidationError):
            schemas.SearchEndpointSchema().load(params)

    def test_only_required_param_sucess(self):

        params = {'q': 'some text'}

        expected = {'search_text': 'some text'}

        data = schemas.SearchEndpointSchema().load(params)

        assert data == expected

    def test_all_params_sucess(self):

        params = {
            'q': 'some text',
            'provider': 'google',
            'hits': 1,
        }

        expected = {
            'search_text': 'some text',
            'provider': 'google',
            'hits': 1,
        }

        data = schemas.SearchEndpointSchema().load(params)

        assert data == expected


class TestSearchApiResponseSchema(BaseTest):

    def test_output_only_defined_keys(self):

        params = {
            'meta': {
                'search_text': 'some text',
                'provider': 'google',
                'hits': 1,
            },
            'links': [{
                'url': 'http://some.site',
                'insights': ['test'],
            }],
            'alien key': 'alien value'
        }

        expected = dict(params)
        del expected['alien key']

        data = schemas.SearchEndpointResponse().dump(params)

        assert data == expected
