from unittest import mock

from text_insights.clients.crawler import CrawlerClient
from text_insights.text_analyser.spacy_analyser import TextAnalyserException
from text_insights.tests.base import BaseTest


class TestApiView(BaseTest):
    def setup_method(self, method):
        from text_insights import main
        from webtest import TestApp

        app = main({}, **self.settings)
        self.testapp = TestApp(app)

    def test_get_homepage(self):
        self.testapp.get('/', status=200)

    def test_get_health(self):
        self.testapp.get('/health', status=200)

    def test_search_with_no_parameters(self):
        self.testapp.get('/search', status=400)

    def test_search_with_missing_required_parameters(self):
        self.testapp.get('/search?text=some+text', status=400)

    @mock.patch.object(CrawlerClient, 'crawl', return_value=[])
    def test_search_with_only_required_parameters(self, crawl_mock):
        res = self.testapp.get('/search?q=some+text', status=200).json

        crawl_mock.assert_called_once_with(search_text='some text')

        assert res['meta'] == {'search_text': 'some text'}
        assert res['links'] == []

    @mock.patch.object(CrawlerClient, 'crawl', return_value=[])
    def test_search_with_all_parameters(self, crawl_mock):
        res = self.testapp.get('/search?q=some+text&provider=google&hits=10', status=200).json

        crawl_mock.assert_called_once_with(hits=10, provider='google', search_text='some text')

        assert res['meta'] == {'search_text': 'some text', 'provider': 'google', 'hits': 10}
        assert res['links'] == []

    @mock.patch('text_insights.api.views.get_insights', return_value=['test'])
    @mock.patch.object(CrawlerClient, 'crawl')
    def test_search_receiving_content_to_analyse(self, crawl_mock, get_insights_mock):
        crawl_mock.return_value = [{
            'url': 'http://some.site',
            'provider': 'google',
            'content': 'This is a fake content'
        }]

        res = self.testapp.get('/search?q=some+text&provider=google&hits=10', status=200).json

        crawl_mock.assert_called_once_with(hits=10, provider='google', search_text='some text')

        assert res['meta'] == {'search_text': 'some text', 'provider': 'google', 'hits': 10}
        assert res['links'] == [{
            'url': 'http://some.site',
            'insights': ['test'],
        }]

    @mock.patch('text_insights.api.views.get_insights', side_effect=TextAnalyserException('Weird error'))
    @mock.patch.object(CrawlerClient, 'crawl')
    def test_search_error_on_analyse_content(self, crawl_mock, get_insights_mock):
        crawl_mock.return_value = [{
            'url': 'http://some.site',
            'provider': 'google',
            'content': 'This is a fake content'
        }]

        res = self.testapp.get('/search?q=some+text&provider=google&hits=10', status=200).json

        crawl_mock.assert_called_once_with(hits=10, provider='google', search_text='some text')

        assert res['meta'] == {'search_text': 'some text', 'provider': 'google', 'hits': 10}
        assert res['links'] == [{
            'url': 'http://some.site',
            'error': 'Weird error',
        }]
