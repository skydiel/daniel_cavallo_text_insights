import spacy
from collections import Counter
from spacy.matcher import Matcher


nlp = spacy.load('en_core_web_sm')


class TextAnalyserException(Exception):
    pass


def get_insights(content):
    nlp = spacy.load('en_core_web_sm')

    try:
        content_doc = nlp(content)
    except ValueError as exc:
        raise TextAnalyserException(str(exc))

    substantives_doc = _get_substantives(content_doc)

    filtered_words = [_preprocess_token(token) for token in substantives_doc if len(token.text) >= 3]

    word_freq = Counter(filtered_words)

    return [word for word, _ in word_freq.most_common(5)]


def _is_token_allowed(token):
    """Only allow valid tokens which are not stop word and punctuation symbols.
    """
    if not token or not token.string.strip() or token.is_stop or token.is_punct:
        return False
    return True


def _preprocess_token(token):
    # Reduce token to its lowercase lemma form
    return token.lemma_.strip().lower()


def _get_substantives(content_doc):
    matcher = Matcher(nlp.vocab)
    matcher.add('SUBSTANTIVES', None, [{'POS': 'NOUN'}])
    for match_id, start, end in matcher(content_doc):
        span = content_doc[start:end]
        yield span
