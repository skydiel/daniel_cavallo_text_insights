import requests


class CrawlerClient:
    def __init__(self, settings):
        crawler_host = settings.get('crawler.host', 'http://localhost:8000')
        self.crawler_api = f'{crawler_host}/crawl'

    def crawl(self, search_text, hits=5):
        res = requests.get(self.crawler_api, params={'q': search_text, 'hits': hits})
        res.raise_for_status()

        return res.json()[:hits]
