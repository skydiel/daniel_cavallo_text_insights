from marshmallow.exceptions import ValidationError
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPBadRequest

from .schemas import SearchEndpointSchema, SearchEndpointResponse
from text_insights.clients.crawler import CrawlerClient
from text_insights.text_analyser.spacy_analyser import TextAnalyserException
from text_insights.text_analyser import get_insights


class TextInsightsViews:

    def __init__(self, request):
        self.request = request
        self.settings = request.registry.settings

    @view_config(route_name="home", request_method="GET", renderer='json')
    def home(self):
        import yaml
        return yaml.load(open('text_insights/api/swagger.yaml', 'r'))

    @view_config(route_name="health", request_method="GET", renderer='string')
    def health(self):
        return 'No need to worry about me. I am very well.'

    @view_config(route_name="search", request_method="GET", renderer='json')
    def search(self):

        try:
            params = SearchEndpointSchema().load(data=dict(self.request.GET))

        except ValidationError as exc:
            raise HTTPBadRequest(str(exc))

        crawled_sites = CrawlerClient(self.settings).crawl(**params)
        for site in crawled_sites:
            try:
                site['insights'] = get_insights(site['content'])

            except TextAnalyserException as exc:
                site['error'] = str(exc)

        resp = {
            'meta': params,
            'links': crawled_sites
        }
        return SearchEndpointResponse().dump(resp)
