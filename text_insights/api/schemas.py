from marshmallow import Schema, fields


class SearchEndpointSchema(Schema):
    """Validate and convert parameters sent to search endpoint.

    q: text to be queried against the providers
    providers: a list separated by comma of search engines to perform the query
    hits: number of top hits to be used for text insights lookup
    """
    search_text = fields.Str(required=True, data_key='q')
    provider = fields.Str()
    hits = fields.Integer()


class SearchEndpointMetaSchema(Schema):
    search_text = fields.Str(required=True)
    provider = fields.Str()
    hits = fields.Integer()


class SearchEnpointLinkSchema(Schema):
    name = fields.Str(required=True)
    url = fields.URL(required=True)
    insights = fields.List(fields.Str())
    error = fields.Str()


class SearchEndpointResponse(Schema):
    meta = fields.Nested(SearchEndpointMetaSchema)
    links = fields.Nested(SearchEnpointLinkSchema, many=True)
