from pyramid.config import Configurator


def main(global_config, **settings):
    config = Configurator(settings=settings)

    config.add_route('home', '/')
    config.add_route('health', '/health')
    config.add_route('search', '/search')

    config.scan('.api.views')

    return config.make_wsgi_app()
