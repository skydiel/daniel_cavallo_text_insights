# Text Insights

A service to find websites based on a search string and generate
keywords that are most representative of their content.

## Installation

This project requires Python 3+.

### Using virtual environment

You can use whatever virtualenv manager you like. The one used here is just a suggestion.

1. First create a virtualenv
```
python3 -m venv .venv
source .venv/bin/activate
```
2. Run the installer
```
make install
```
3. Run the application
```
make run
```

### Using Docker container

Make sure you have [Docker](https://docs.docker.com/v17.12/install/) installed on your machine.

1. Build the application image
```
docker build --tag text-insights .
```
2. Run a containerized instance of the application
```
docker run -d --rm --name text-insights-app -p 8100:8100 -e ENVIRONMENT=development text-insights
```

## Usage

This service depends on another service to provide the content to be used on generation of text insights.
The URL of this "content provider" is defined in the `ini` files by the property `crawler.host`. It is expected that the content provider exposes an endpoint called `crawl` and accepts the following parameters in the query string:

- q: the text to be used in the search
- hits: how many sites should br crawled

## API Documentation

If you are running this service locally, on the default port, you can view the API documentation on
http://localhost:8000/.

The document returned can be better viewed on https://editor.swagger.io/.

### Limitation

Content greater than 1MM characters are not allowed. If Text-Insights receives a response where there are
larger contents, this particular document will appear in the response with an attribute `error` with a
description of what went wrong.
