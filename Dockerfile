FROM gigantum/python3-minimal:17b8e50607-2019-02-08

# Create application directory
WORKDIR /app
COPY . .

RUN chmod u+x run.sh

# Installing dependences
RUN make install


# Network
EXPOSE 8100

CMD [ "bash" , "run.sh"]
